import java.io.*;
import java.util.Arrays;
import java.util.Collections;

/**
 * Задача "Заказы", принимающая исходные данные из файла "test.txt"
 * и записывающая результаты в файл "results.txt"
 * @author Chelnokov E.I., 16IT18K
 */
public class Orders {
    public static void main(String[] args)throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\test.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\results.txt"));
        int sum;
        int time;
        String timeString = bufferedReader.readLine();// начальные значения, т.е. первые строчки
        String ordersString = bufferedReader.readLine();
        while (timeString != null && ordersString != null) {
            time = Integer.valueOf(timeString);
            System.out.println("Время на выполнение: " + time);
            String[] ordersStringArray = ordersString.split(" ");
            Integer [] orders = new Integer[ordersStringArray.length];
            for (Integer i = 0;i < orders.length ; i++){// записывает поштучно данные из строкового массива в целочисленный
                orders[i] = Integer.valueOf(ordersStringArray[i]);
            }
            if (orders.length < time) {
                sum = arraySum(orders, orders.length);
            } else {
                Arrays.sort(orders, Collections.reverseOrder());
                sum = arraySum(orders, time);
            }
            System.out.println("Заработок с самых дорогих заказов: " + sum);
            timeString = bufferedReader.readLine();
            ordersString = bufferedReader.readLine();
            bufferedWriter.write(sum);
        }
        bufferedReader.close();
        bufferedWriter.close();
    }

    private static int arraySum(Integer arr[], int t) {
        int sum = 0;
        for(int i = 0; i < t; i++){
            sum = sum + arr[i];
        }
        return  sum;
    }
}